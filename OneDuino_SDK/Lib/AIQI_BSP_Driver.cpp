/*
 * 有些时序严格函数需要关闭中断，防止被spi影响
 */

#include "AIQI_BSP_Driver.h"
#include "Arduino.h"
#include "SPI.h"
uint8_t TYPE_NUM=0x00;//类型号  
uint8_t METHOD_NUM=0x00;//方法号 

uint8_t Read_Bytes[3]={0};
uint8_t Write_Bytes[3]={0};

uint8_t Data_Direction_Flag=0;//读写主控配置 0：主控->传感器 1：传感器->主控

/****************************************************************************
作    者：lty
日   期：2019.12.14
函数功能：三线转发
输入参数：传感器名 见AIQI_BSP_Driver.h
输出参数：void
备   注：
*****************************************************************************/
void AIQI_BSP_Init(uint8_t Mx_Num)
{
  switch(Mx_Num)
  {
    case ONEBOT_BEEP:
      TYPE_NUM=TYPE_BEEP;//类型号  
      METHOD_NUM=METHOD_BEEP;//方法号 
	    Data_Direction_Flag=0;//模块方向
    break;
    case ONEBOT_LED_8C:
      TYPE_NUM=TYPE_LED;//类型号  
      METHOD_NUM=METHOD_LED;//方法号
	    Data_Direction_Flag=0;//模块方向
    break;
    case ONEBOT_RGB_LED:
      TYPE_NUM=TYPE_RGB_LED;//类型号  
      METHOD_NUM=METHOD_RGB_LED;//方法号 
	    Data_Direction_Flag=0;//模块方向
    break;
    case ONEBOT_HALL:
      TYPE_NUM=TYPE_HALL;//类型号  
      METHOD_NUM=METHOD_HALL;//方法号 
	    Data_Direction_Flag=1;//模块方向
    break;
    case ONEBOT_LIGHT:
      TYPE_NUM=TYPE_LIGHT;//类型号  
      METHOD_NUM=METHOD_LIGHT;//方法号 
	    Data_Direction_Flag=1;//模块方向
    break;
    case ONEBOT_KEY:
      TYPE_NUM=TYPE_KEY_State;//类型号  
      METHOD_NUM=METHOD_KEY_State;//方法号 
	    Data_Direction_Flag=1;//模块方向
    break;
    case ONEBOT_TILT:
      TYPE_NUM=TYPE_TILT;//类型号  
      METHOD_NUM=METHOD_TILT;//方法号 
	    Data_Direction_Flag=1;//模块方向
    break;
    case ONEBOT_GAS:
      TYPE_NUM=TYPE_GAS;//类型号  
      METHOD_NUM=METHOD_GAS;//方法号 
	    Data_Direction_Flag=1;//模块方向
    break;
    case ONEBOT_TOUCH_KEY:
      TYPE_NUM=TYPE_TOUCH_KEY;//类型号  
      METHOD_NUM=METHOD_TOUCH_KEY;//方法号 
	    Data_Direction_Flag=1;//模块方向
    break;
    case ONEBOT_WIFI:
      TYPE_NUM=TYPE_WIFI_Model;//类型号  
      METHOD_NUM=METHOD_WIFI_GetData;//方法号
	    Data_Direction_Flag=0;//模块方向
    break;
    case ONEBOT_DIG_DISP:
      TYPE_NUM=TYPE_DIG_DISP;//类型号  
      METHOD_NUM=METHOD_DIG_DISP;//方法号 
	    Data_Direction_Flag=0;//模块方向
    break;
    case ONEBOT_BOARD:
      TYPE_NUM=TYPE_BOARD;//类型号  
      METHOD_NUM=METHOD_RGB_LED;//方法号 
	    Data_Direction_Flag=0;//模块方向
    break;
    case ONEBOT_PIR:
      TYPE_NUM=TYPE_PIR;//类型号  
      METHOD_NUM=METHOD_PIR;//方法号 
	    Data_Direction_Flag=1;//模块方向
    break;
    case ONEBOT_ST:
      TYPE_NUM=TYPE_SMOKE;//类型号  
      METHOD_NUM=METHOD_SMOKE;//方法号 
	    Data_Direction_Flag=1;//模块方向
    break;
    case ONEBOT_RR:
      TYPE_NUM=TYPE_GateMagnetic;//类型号  
      METHOD_NUM=METHOD_GateMagnetic;//方法号 
	    Data_Direction_Flag=1;//模块方向
    break;
    case ONEBOT_LE:
      TYPE_NUM=TYPE_LaserEmission;//类型号  
      METHOD_NUM=METHOD_LaserEmission;//方法号 
	    Data_Direction_Flag=0;//模块方向
    break;
    case ONEBOT_LR:
      TYPE_NUM=TYPE_LaserInduction;//类型号  
      METHOD_NUM=METHOD_LaserInduction;//方法号 
	    Data_Direction_Flag=1;//模块方向
    break;
    case ONEBOT_AP:
      TYPE_NUM=TYPE_AtmosphericPressure;//类型号  
      METHOD_NUM=METHOD_AtmosphericPressure;//方法号 
      Data_Direction_Flag=1;//模块方向
    break;
    case ONEBOT_TH:
      TYPE_NUM=TYPE_TEMP_HUMI;//类型号  
      METHOD_NUM=METHOD_TEMP;//方法号 
	    Data_Direction_Flag=1;//模块方向
    break;
  }
  SPI_Init();
}
/****************************************************************************
作    者：lty
日   期：2019.12.17
函数功能：SPI初始化
输入参数：void
输出参数：void
备   注：
*****************************************************************************/
void SPI_Init(void)
{
   pinMode(MISO, OUTPUT); // have to send on master in so it set as output
   SPCR |= _BV(SPE); // turn on SPI in slave mode
   SPISettings(15000000, MSBFIRST, SPI_MODE0);
   SPI.attachInterrupt(); // turn on interrupt
}
/****************************************************************************
作    者：lty
日   期：2019.12.17
函数功能：桥接IC SPI通信中断
输入参数：void
输出参数：void
备   注：内部时序严格
*****************************************************************************/
ISR (SPI_STC_vect) // SPI interrupt routine
{
  uint8_t Comm,Verify,i;
  uint8_t Rvalue[3];
  uint8_t Tvalue[3]={0};
  
  Comm = SPI.transfer(0xAC);//接收指令并回复确认码
  SPI.transfer(METHOD_NUM);//回复当前方法号
  SPI.transfer(Data_Direction_Flag);//回复当前方法方向 0：主控->传感器 1：传感器->主控
  switch(Comm)
  {
    case 0x01:{//注册指令
      Tvalue[0]=TYPE_NUM;//类型号
        for(i=0;i<3;i++)
      {
        Rvalue[i] = SPI.transfer(Tvalue[i]);
      }
      break;
    }
    case 0x04:{//交换数据指令
      memcpy(Tvalue,Write_Bytes,3);
      for(i=0;i<3;i++)
      {
        Rvalue[i] = SPI.transfer(Tvalue[i]);//交换三个字节数据
      }
      break; 
    }
  }
    Verify=(Rvalue[0]^Rvalue[1]^Rvalue[2]^Tvalue[0]^Tvalue[1]^Tvalue[2]);
    if(SPI.transfer(Verify)==Verify)//校验有效
    {
      memcpy(Read_Bytes,Rvalue,3);//将接收到的三字节保存到缓存区
    }else{
      Serial.println("丢包！");
      //丢包执行的函数
    }
  }



