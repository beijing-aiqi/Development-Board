#ifndef _AIQI_BSP_DRIVER_H_
#define _AIQI_BSP_DRIVER_H_
#include "Arduino.h"//导入Arduino核心头文件


////模块定义
#define   ONEBOT_BEEP                    1//蜂鸣器
#define   ONEBOT_LED_8C                  2//8个LED
#define   ONEBOT_RGB_LED                 3//RGB LED
#define   ONEBOT_HALL                    5//霍尔
//#define   ONEBOT_FIRE                  6//火焰 未启用
#define   ONEBOT_LIGHT                   7//光敏
#define   ONEBOT_KEY                     8//按键
#define   ONEBOT_TILT                    9//倾斜
//#define   ONEBOT_SHAKE                 10//震动 未启用
#define   ONEBOT_GAS                     11//天然气
//#define   ONEBOT_OPTO_KEY              12//光耦 未启用
#define   ONEBOT_TOUCH_KEY               13//触摸
#define   ONEBOT_WIFI                    14//无线esp8266
//#define   ONEBOT_RELAY                 15//继电器 未启用
#define   ONEBOT_DIG_DISP                16//数码管
#define   ONEBOT_BOARD                   17//二次开发板
#define   ONEBOT_PIR                     18//人体红外
#define   ONEBOT_ST                      19//烟雾
#define   ONEBOT_RR                      20//门磁
#define   ONEBOT_LE                      21//激光发射
#define   ONEBOT_LR                      22//激光接收
#define   ONEBOT_AP                      23//气压
#define   ONEBOT_TH                      24//数字温湿度
#define   ONEBOT_IR                      25//红外遥控
#define   ONEBOT_RFID                    26//RFID标签



extern uint8_t TYPE_NUM;//类型号
extern uint8_t METHOD_NUM;//方法号
extern uint8_t Read_Bytes[3];//读缓存区
extern uint8_t Write_Bytes[3];//写缓存区

enum TYPE_NUM//类型号
{
  TYPE_HALL = 0x5a,                   //霍尔
  TYPE_LIGHT = 0x5b,                  //光敏
  TYPE_KEY_State = 0x5c,              //按键状态
  TYPE_TILT = 0x5d,                   //倾斜开关
  TYPE_SHAKE = 0x5e,                  //振动
  TYPE_GAS = 0x5f,                    //天然气
  TYPE_OPTO_KEY = 0x60,               //光电开关
  TYPE_TOUCH_KEY = 0x61,              //触摸开关
  TYPE_BEEP = 0x73,                   //蜂鸣器
  TYPE_LED = 0x74,                    //LED
  TYPE_RGB_LED = 0x75,                //RGB
  TYPE_DIG_DISP = 0x76,               //数码管
  TYPE_WIFI_Model= 0x77,             //WiFi
  TYPE_RELAY = 0x78,                  //继电器
  TYPE_BOARD = 0x8a,                     //二次开发板
  TYPE_PIR = 0x62,                    //红外热释电
  TYPE_SMOKE = 0x63,                  //烟雾
  TYPE_GateMagnetic = 0x64,           //门磁干簧管
  TYPE_LaserEmission = 0x7c,          //激光发射
  TYPE_LaserInduction = 0x65,         //激光接收
  TYPE_AtmosphericPressure = 0x66,    //大气压强
  TYPE_TEMP_HUMI = 0x59,              //温湿度
  

};

enum METHOD_NUM//方法号
{
  METHOD_TEMP = 5,                    //5 温度  1
  METHOD_HUMI,                        //6 湿度  1
  METHOD_HALL,                        //7 霍尔  1
  METHOD_LIGHT,                       //8 光敏  2: 高位字节  低位字节
  METHOD_TILT,                        //9 倾斜开关  1
  METHOD_SHAKE,                       //10 振动   1
  METHOD_GAS,                         //11 天然气 2: 高位字节  低位字节
  METHOD_OPTO_KEY,                    //12 光电开关 1
  METHOD_TOUCH_KEY,                   //13 触摸开关 1
  METHOD_BEEP,                        //14 蜂鸣器 1
  METHOD_LED,                         //15 LED  1
  METHOD_DIG_DISP,                    //16 数码管 2: 高位字节  低位字节
  METHOD_RELAY,                       //17 继电器 1
  METHOD_WIFI_GetData,                //18 WiFi 访问数据 3
  METHOD_WIFI_CtrlData,               //19 WiFi 控制数据 3
  METHOD_PIR = 26,                    //26 红外热释电 1
  METHOD_SMOKE,                       //27 烟雾 3
  METHOD_GateMagnetic,                //28 门磁干簧管 1
  METHOD_LaserEmission,               //29 激光发射 1
  METHOD_LaserInduction,              //30 激光接收 1
  METHOD_AtmosphericPressure,         //31 大气压强 3
  METHOD_KEY_State = 109,             //109 按键状态  1 
  METHOD_RGB_LED = 117                    //117 RGB LED  3
};

void AIQI_BSP_Init(uint8_t Mx_Num);
void SPI_Init(void);

#endif
