
#include "AIQI_BSP_Driver.h"
#include "OneDuino_TemperatureHumidity.h"

#define AHT10_Address 0x38  //AHT10 Device地址

/****************************************************************************
函数功能：iic初始化
输入参数：None
输出参数：None
*****************************************************************************/
void IIC_Init(void)
{
   Wire.begin();
   Wire.setClock(100000);
}
/****************************************************************************
函数功能：复位
输入参数：None
输出参数：None
*****************************************************************************/
void AIQI_Send_BA(void)//向AHT10发送BA命令 复位
{
  Wire.beginTransmission(AHT10_Address);//发送起始信号和I2C地址
  Wire.write(0xBA);//发送数据
  Wire.endTransmission();//发送停止信号
}
/****************************************************************************
函数功能：读状态位
输入参数：None
输出参数：状态位
*****************************************************************************/
uint8_t AIQI_Read_Status(void)
{
  uint8_t Byte_first; 
  Wire.requestFrom(AHT10_Address,1);//主机用于从从设备请求字节。
  if(Wire.available()==1)//检索请求的字节数
  {
    Byte_first=Wire.read();
  }
  return Byte_first;
}
/****************************************************************************
函数功能：读校验位
输入参数：None
输出参数：1可以操作 0未校验
*****************************************************************************/
uint8_t AIQI_Read_Cal_Enable(void)  //查询cal enable位有没有使能
{
  uint8_t val = 0;//ret = 0,
 
  val = AIQI_Read_Status();
  //Serial.print("val=");
  //Serial.print(val,HEX);
  if((val & 0x68)==0x08)  //判断NOR模式和校准输出是否有效
  return 1;
  else  return 0;
 }
/****************************************************************************
函数功能：温湿度传感器初始化
输入参数：None
输出参数：None
*****************************************************************************/
void AIQI_TH_Init(void)
{
    IIC_Init();
    delay(20);
    Wire.beginTransmission(AHT10_Address);//发送起始信号和I2C地址
    Wire.write(0xE1);//发送数据
    Wire.write(0x08);//发送数据
    Wire.write(0x00);//发送数据
    Wire.endTransmission();//发送停止信号
    delay(100);
  while(AIQI_Read_Cal_Enable()==0)//需要等待状态字status的Bit[3]=1时才去读数据。如果Bit[3]不等于1 ，发软件复位0xBA给AHT10，再重新初始化AHT10，直至Bit[3]=1
   {
    AIQI_Send_BA();  //复位
    delay(100);
    //Serial.print("复位\r\n");
    Wire.beginTransmission(AHT10_Address);//发送起始信号和I2C地址
    Wire.write(0xE1);//发送数据
    Wire.write(0x08);//发送数据
    Wire.write(0x00);//发送数据
    Wire.endTransmission();//发送停止信号
    delay(100);
   }
}
/****************************************************************************
函数功能：读温湿度
输入参数：None
输出参数：float 温湿度值
*****************************************************************************/

float T=0,H=0;

void AIQI_Read_RH(void)
{
  uint32_t RetuData=0;
  uint8_t TH_Data[6];
  uint16_t cnt = 0;
  while(AIQI_Read_Cal_Enable()==0)//等到校准输出使能位为1，才读取。
   {
    AIQI_TH_Init();//如果为0再使能一次
    delay(30);
   }
  Wire.beginTransmission(AHT10_Address);//发送起始信号和I2C地址
  Wire.write(0xAC);//发送数据
  Wire.write(0x33);//发送数据
  Wire.write(0x00);//发送数据
  Wire.endTransmission();//发送停止信号
  delay(80);//延时间隔最低75ms
  while(((AIQI_Read_Status()&0x01)==0x01))//等待忙状态结束
  {
    delay(2);
    if(cnt++>=100)//防止无限等待
    {
    break;
    }
  }
  Wire.beginTransmission(AHT10_Address);//发送起始信号和I2C地址
  Wire.write(0xAC);//发送数据
  Wire.write(0x33);//发送数据
  Wire.write(0x00);//发送数据
  Wire.endTransmission();//发送停止信号
  delay(80);
  Wire.requestFrom(AHT10_Address,6);//主机用于从从设备请求字节。
  if(Wire.available()==6)//检索请求的字节数
  {
        TH_Data[0] = Wire.read();
        TH_Data[1] = Wire.read();
        TH_Data[2] = Wire.read();
        TH_Data[3] = Wire.read();
        TH_Data[4] = Wire.read();
        TH_Data[5] = Wire.read();
        RetuData = (RetuData|TH_Data[1])<<8;
        RetuData = (RetuData|TH_Data[2])<<8;
        RetuData = (RetuData|TH_Data[3]);
        RetuData = RetuData >>4;
        H = RetuData;//湿度原始数值
        RetuData = 0;
        RetuData = (RetuData|TH_Data[3])<<8;
        RetuData = (RetuData|TH_Data[4])<<8;
        RetuData = (RetuData|TH_Data[5]);
        RetuData = RetuData&0xfffff;
        T = RetuData;//温度原始数值
        RetuData = 0;
        H=(H/1024/1024*100)*10;    //计算得到湿度值（放大了10倍,如果c1=523，表示现在湿度为52.3%）
        T=((T/1024/1024)*200-50)*10;//计算得到温度值（放大了10倍，如果t1=245，表示现在温度为24.5℃）
        AIQI_Send_BA();
  }  
}
/****************************************************************************
函数功能：读湿度
输入参数：None
输出参数：float 湿度
*****************************************************************************/
float AIQI_Read_Humidity(void)
{
    AIQI_Read_RH();
    return H/10;
}
/****************************************************************************
函数功能：读温度
输入参数：None
输出参数：float 温度
*****************************************************************************/
float AIQI_Read_Temp(void)
{
    AIQI_Read_RH();
    return T/10;
}

/****************************************************************************
函数功能：向主控发送湿度
输入参数：湿度值
输出参数：none
*****************************************************************************/
void AIQI_Write_Humidity(float Hum)
{
  if(METHOD_NUM != METHOD_HUMI)
  {
    METHOD_NUM=METHOD_HUMI;//修改传输方法号，将要传输湿度数据给主控
  }
    AIQI_Write_Bytes[1]=Hum;//将湿度发给主控
    AIQI_Write_Bytes[2]=(int)(Hum*100)%100;//小数部分 取两位
}

/****************************************************************************
函数功能：向主控发送温度
输入参数：温度值
输出参数：none
*****************************************************************************/
void AIQI_Write_Temp(float Temp)
{
    if(METHOD_NUM != METHOD_TEMP)
  {
    METHOD_NUM=METHOD_TEMP;//修改参数方法号，将要传输温度数据给主控
  }
    AIQI_Write_Bytes[1]=Temp;//将温度发给主控
    AIQI_Write_Bytes[2]=(int)(Temp*100)%100*10;//小数部分 取两位
}
