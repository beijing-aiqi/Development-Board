#include "AIQI_BSP_Driver.h"
#include "OneDuino_NixieTube.h"//数码管驱动

void setup (void) 
{
   AIQI_BSP_Init(ONEBOT_DIG_DISP);//AIQI硬件驱动初始化
   AIQI_Num_Display_Init();//数码管初始化
   AIQI_Num_DisPlay_Level(5);//数码管亮度设置
}


  void loop (void) 
{
  uint16_t i,num = 123;  // 数码管要显示的数字 
  if( (AIQI_Read_Bytes) != 0 )   //如果主控发送了控制数据
  {
    num = AIQI_Read_Bytes[0];//读取主控发送的数的高八位
    num <<=8;
    num +=AIQI_Read_Bytes[1];//读取低八位 
  }
    AIQI_Num_DisPlay_ThreeBit(num);//数码管显示
}
