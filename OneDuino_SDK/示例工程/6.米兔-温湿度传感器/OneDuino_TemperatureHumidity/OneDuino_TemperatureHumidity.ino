#include "AIQI_BSP_Driver.h"
#include "OneDuino_TemperatureHumidity.h"

void setup (void) 
{
   AIQI_BSP_Init(ONEBOT_TH);   //AIQI硬件驱动初始化
   AIQI_TH_Init();      //模块初始化
}

void loop (void)
{
  AIQI_Write_Humidity(AIQI_Read_Humidity());//获取湿度值并发送给主控
  AIQI_Write_Temp(AIQI_Read_Temp());//获取温度值并发送给主控
}
