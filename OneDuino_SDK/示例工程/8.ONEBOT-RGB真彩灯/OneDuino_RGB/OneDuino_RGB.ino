#include "AIQI_BSP_Driver.h"

void setup (void) 
{
   AIQI_BSP_Init(ONEBOT_RGB_LED);//AIQI硬件驱动初始化
   pinMode(5, OUTPUT);//红色（R）控制
   pinMode(6, OUTPUT);//绿色（G）控制
   pinMode(9, OUTPUT);//蓝色（B）控制
   digitalWrite(5, HIGH);//初始化引脚为高
   digitalWrite(6, HIGH);
   digitalWrite(9, HIGH);
}
void loop (void) 
{
  analogWrite(5, ~AIQI_Read_Bytes[0]);//从主控获取红色（R）数据 并输出pwm信号
  analogWrite(6, ~AIQI_Read_Bytes[1]);//从主控获取绿色（G）数据 并输出pwm信号
  analogWrite(9, ~AIQI_Read_Bytes[2]);//从主控获取蓝色（B）数据 并输出pwm信号
}
