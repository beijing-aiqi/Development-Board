#ifndef _AIQI_BSP_DRIVER_H_
#define _AIQI_BSP_DRIVER_H_
#include "Arduino.h"//导入Arduino核心头文件


////模块定义
#define   ONEBOT_BEEP                    1//蜂鸣器
#define   ONEBOT_LED_8C                  2//8个LED
#define   ONEBOT_RGB_LED                 3//RGB LED
#define   ONEBOT_HALL                    5//霍尔
#define   ONEBOT_LIGHT                   7//光敏
#define   ONEBOT_KEY                     8//按键
#define   ONEBOT_TILT                    9//倾斜
#define   ONEBOT_GAS                     11//天然气
#define   ONEBOT_TOUCH_KEY               13//触摸
#define   ONEBOT_WIFI                    14//无线esp8266
#define   ONEBOT_DIG_DISP                16//数码管
#define   ONEBOT_BOARD                   17//二次开发板
#define   ONEBOT_PIR                     18//人体红外
#define   ONEBOT_ST                      19//烟雾
#define   ONEBOT_RR                      20//门磁
#define   ONEBOT_LE                      21//激光发射
#define   ONEBOT_LR                      22//激光接收
#define   ONEBOT_AP                      23//气压
#define   ONEBOT_TH                      24//数字温湿度
#define   ONEBOT_IR                      25//红外遥控
#define   ONEBOT_RFID                    26//RFID标签

extern uint8_t TYPE_NUM;
extern uint8_t METHOD_NUM;
extern uint8_t AIQI_Read_Bytes[3];//读缓存区
extern uint8_t AIQI_Write_Bytes[3];//写缓存区









enum TYPE_NUM//类型号
{
  TYPE_HALL = 0x5a,                   
  TYPE_LIGHT = 0x5b,                  
  TYPE_KEY_State = 0x5c,              
  TYPE_TILT = 0x5d,                   
  TYPE_SHAKE = 0x5e,                  
  TYPE_GAS = 0x5f,                    
  TYPE_OPTO_KEY = 0x60,              
  TYPE_TOUCH_KEY = 0x61,             
  TYPE_BEEP = 0x73,                  
  TYPE_LED = 0x74,                   
  TYPE_RGB_LED = 0x75,              
  TYPE_DIG_DISP = 0x76,            
  TYPE_WIFI_Model= 0x77,            
  TYPE_RELAY = 0x78,               
  TYPE_BOARD = 0x8a,                  
  TYPE_PIR = 0x62,                 
  TYPE_SMOKE = 0x63,                
  TYPE_GateMagnetic = 0x64,        
  TYPE_LaserEmission = 0x7c,         
  TYPE_LaserInduction = 0x65,        
  TYPE_AtmosphericPressure = 0x66,    
  TYPE_TEMP_HUMI = 0x59,              
  

};

enum METHOD_NUM//方法号
{
  METHOD_TEMP = 5,                    
  METHOD_HUMI,                        
  METHOD_HALL,                       
  METHOD_LIGHT,                      
  METHOD_TILT,                        
  METHOD_SHAKE,                       
  METHOD_GAS,                       
  METHOD_OPTO_KEY,                    
  METHOD_TOUCH_KEY,                  
  METHOD_BEEP,                        
  METHOD_LED,                         
  METHOD_DIG_DISP,                  
  METHOD_RELAY,                       
  METHOD_WIFI_GetData,               
  METHOD_WIFI_CtrlData,               
  METHOD_PIR = 26,                   
  METHOD_SMOKE,                       
  METHOD_GateMagnetic,               
  METHOD_LaserEmission,               
  METHOD_LaserInduction,              
  METHOD_AtmosphericPressure,        
  METHOD_KEY_State = 109,             
  METHOD_RGB_LED = 117                  
};

void AIQI_BSP_Init(uint8_t Mx_Num);
void AIQI_SPI_Init(void);

#endif
