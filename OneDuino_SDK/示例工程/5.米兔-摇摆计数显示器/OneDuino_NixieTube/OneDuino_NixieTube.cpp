/*****************************************************************************************************************************************************
Aip650驱动 单位及三位数码管显示 亮度调节
*****************************************************************************************************************************************************/

#include "OneDuino_NixieTube.h"

//驱动引脚
#define DIO                   14
#define CLK                   15

#define CLK_H                 Aip650_CLK(1)
#define CLK_L                 Aip650_CLK(0)
#define DIO_H                 Aip650_DIO(1)
#define DIO_L                 Aip650_DIO(0)

// 共阴极数码管   段码表:0 1 2 3 4 5 6 7 8 9 A b C d E F H J L P U -
uint8_t DISPLAY_CODE[22]={

0x3f,0x06,0x5b,0x4f,

0x66,0x6d,0x7d,0x07,

0x7f,0x6f,0x77,0x7c,

0x39,0x5e,0x79,0x71,
	
0x76,0x0e,0x38,0x73,0x3e,0x40};
//数码管位数                                 1         2    3           4
uint8_t DIG_BIT_CODE[4]          = {        0x68,0x6a,0x6c,0x6e };
//8段显示亮度等级                           1         2          3           4        5         6    7           8                       
uint8_t Light_Level_CODE[8]        = {        0x11,0x21,0x31,0x41,0x51,0x61,0x71,0x01 };

/****************************************************************************
函数功能：显示输入转换器
输入参数：要显示的数 0 1 2 3 4 5 6 7 8 9 A b C d E F H J L P U -
输出参数：int
*****************************************************************************/
uint8_t To_Display(char num)
{
	switch(num)
	{
		case '0':return 0;
		case '1':return 1;
		case '2':return 2;
		case '3':return 3;
		case '4':return 4;
		case '5':return 5;
		case '6':return 6;
		case '7':return 7;
		case '8':return 8;
		case '9':return 9;
		case 'r':
		case 'R':
		case 'a':
		case 'A':return 10;
		case 'b':
		case 'B':return 11;
		case 'c':
		case 'C':return 12;
		case 'd':
		case 'D':return 13;
		case 'e':
		case 'E':return 14;
		case 'f':
		case 'F':return 15;
		case 'h':
		case 'H':return 16;
		case 'j':
		case 'J':return 17;
		case 'l':
		case 'L':return 18;
		case 'p':
		case 'P':return 19;
		case 'u':
		case 'U':return 20;
		case '-':return 21;
	}
	return 0;
}

/****************************************************************************
函数功能：初始化
输入参数：None
输出参数：None
*****************************************************************************/
void AIQI_Num_Display_Init(void)
{
  pinMode(DIO,OUTPUT);
  pinMode(CLK,OUTPUT);
}
/****************************************************************************
函数功能：CLK控制
输入参数：HL 高低电平
输出参数：None
*****************************************************************************/
void Aip650_CLK(uint8_t HL)
{
	switch (HL)
	{
		case 1:
			digitalWrite(CLK, HIGH);
			break;
		case 0:
			digitalWrite(CLK, LOW);
			break;
	}
}
/****************************************************************************
函数功能：DIO控制
输入参数：HL 高低电平
输出参数：None
*****************************************************************************/
void Aip650_DIO(uint8_t HL)
{
	switch (HL)
	{
		case 1:
			digitalWrite(DIO, HIGH);
                        break;
                case 0:
                        digitalWrite(DIO, LOW);
			break;
	}
}
/****************************************************************************
函数功能：开始 回复 结束信号
输入参数：None
输出参数：None
*****************************************************************************/
void I2CStart(void)//开始信号
{
        CLK_H;
        DIO_H;
        delayMicroseconds(5);
        DIO_L;
}

void I2Cask(void) //ACK信号延时，未作处理
{
        CLK_H;
	delayMicroseconds(5);
        CLK_L;
        delayMicroseconds(5);
        CLK_L;
}

void I2CStop(void) //停止信号
{
        CLK_H;
        DIO_L;
        delayMicroseconds(5);
        DIO_H;
}
/****************************************************************************
函数功能：写一个字节高位在前，低位在后
输入参数：OneByte 写字节
输出参数：None
*****************************************************************************/
void I2CWrByte(uint8_t OneByte) 
{
        uint8_t i;
        CLK_L;
	delayMicroseconds(1);//CLK下降沿延时，防止下降沿改变DIO
        for(i=0;i<8;i++)
        {
		(OneByte & 0x80) ? DIO_H: DIO_L;
                OneByte = OneByte<<1;
                CLK_L;
                delayMicroseconds(5);
                CLK_H;
                delayMicroseconds(5);
                CLK_L;
        }
}
/****************************************************************************
函数功能：发送一条指令 两个字节
输入参数：add 高字节，dat 低字节
输出参数：None
*****************************************************************************/
void AiP650_Set(uint8_t add,uint8_t dat)
{
        //写显存必须从高地址开始写
        I2CStart();
        I2CWrByte(add); 
        I2Cask();
        I2CWrByte(dat);
        I2Cask();
        I2CStop();
}
/****************************************************************************
函数功能：显示一位数字
输入参数：DIG_Bit 显示位，Display_num 数字，DP 是否显示小数点
输出参数：None
*****************************************************************************/
void AIQI_Num_DisPlay_OneBit(uint8_t DIG_Bit, uint8_t Display_num, uint8_t DP)             
{
	if(DP==1){
		AiP650_Set(DIG_BIT_CODE[DIG_Bit-1],DISPLAY_CODE[Display_num]|0x80);
	}else{
        AiP650_Set(DIG_BIT_CODE[DIG_Bit-1],DISPLAY_CODE[Display_num]);
	}
}
/****************************************************************************
函数功能：清屏
输入参数：None
输出参数：None
*****************************************************************************/
void AIQI_Num_DisPlay_CLR(void)
{
        uint8_t i;
        for(i=0;i<4;i++)
        {
                AiP650_Set(DIG_BIT_CODE[i],0x00);               
        }
}
/****************************************************************************
函数功能：显示一个3位数 （0-999）
输入参数：Display_num 三位数字
输出参数：None
*****************************************************************************/
void AIQI_Num_DisPlay_ThreeBit(uint16_t Display_num)                
{
        uint8_t One,Two,Three;
        One   = Display_num/100;
        Two   = Display_num%100/10;
        Three = Display_num%10;
        if(One == 0)
        {
                AiP650_Set(DIG_BIT_CODE[0],0x00);        
                if(Two == 0)
                {
                        AiP650_Set(DIG_BIT_CODE[1],0x00);
                        if(Three == 0)
                        {
                                AiP650_Set(DIG_BIT_CODE[2],0x00);
                        }
                        else
                        {
                                AIQI_Num_DisPlay_OneBit(3,Three,0);
                        }
                }
                else
                {
                        AIQI_Num_DisPlay_OneBit(2,Two,0);
                        AIQI_Num_DisPlay_OneBit(3,Three,0);       
                }
        }
        else  
        {
                AIQI_Num_DisPlay_OneBit(1,One,0);
                AIQI_Num_DisPlay_OneBit(2,Two,0);
                AIQI_Num_DisPlay_OneBit(3,Three,0);
        }                        
}
/****************************************************************************
函数功能：设置亮度等级1-8级 并使能
输入参数：Level 亮度（1-8）
输出参数：None
*****************************************************************************/
void AIQI_Num_DisPlay_Level(uint8_t Level)                                           
{
        AiP650_Set(0x48,Light_Level_CODE[Level-1]);
}
