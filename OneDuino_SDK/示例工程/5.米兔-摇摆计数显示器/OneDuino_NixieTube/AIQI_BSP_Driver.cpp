/*
 * 桥接IC SPI通信协议
 */

#include "AIQI_BSP_Driver.h"
#include "Arduino.h"
#include "SPI.h"
uint8_t TYPE_NUM=0x00;
uint8_t METHOD_NUM=0x00;

uint8_t AIQI_Read_Bytes[3]={0};
uint8_t AIQI_Write_Bytes[3]={0};

uint8_t AIQI_Data_Direction_Flag=0;//读写主控配置 0：主控->传感器 1：传感器->主控

/****************************************************************************
函数功能：选择传感器
输入参数：传感器名 见AIQI_BSP_Driver.h
输出参数：void
备   注：
*****************************************************************************/
void AIQI_BSP_Init(uint8_t Mx_Num)
{
  switch(Mx_Num)
  {
    case ONEBOT_BEEP:
      TYPE_NUM=TYPE_BEEP; 
      METHOD_NUM=METHOD_BEEP;
      AIQI_Data_Direction_Flag=0;
    break;
    case ONEBOT_LED_8C:
      TYPE_NUM=TYPE_LED;
      METHOD_NUM=METHOD_LED;
      AIQI_Data_Direction_Flag=0;
    break;
    case ONEBOT_RGB_LED:
      TYPE_NUM=TYPE_RGB_LED;  
      METHOD_NUM=METHOD_RGB_LED;
      AIQI_Data_Direction_Flag=0;
    break;
    case ONEBOT_HALL:
      TYPE_NUM=TYPE_HALL;
      METHOD_NUM=METHOD_HALL;
      AIQI_Data_Direction_Flag=1;
    break;
    case ONEBOT_LIGHT:
      TYPE_NUM=TYPE_LIGHT; 
      METHOD_NUM=METHOD_LIGHT;
      AIQI_Data_Direction_Flag=1;
    break;
    case ONEBOT_KEY:
      TYPE_NUM=TYPE_KEY_State; 
      METHOD_NUM=METHOD_KEY_State;
      AIQI_Data_Direction_Flag=1;
    break;
    case ONEBOT_TILT:
      TYPE_NUM=TYPE_TILT; 
      METHOD_NUM=METHOD_TILT;
      AIQI_Data_Direction_Flag=1;
    break;
    case ONEBOT_GAS:
      TYPE_NUM=TYPE_GAS; 
      METHOD_NUM=METHOD_GAS;
      AIQI_Data_Direction_Flag=1;
    break;
    case ONEBOT_TOUCH_KEY:
      TYPE_NUM=TYPE_TOUCH_KEY;  
      METHOD_NUM=METHOD_TOUCH_KEY; 
      AIQI_Data_Direction_Flag=1;
    break;
    case ONEBOT_WIFI:
      TYPE_NUM=TYPE_WIFI_Model;  
      METHOD_NUM=METHOD_WIFI_GetData;
      AIQI_Data_Direction_Flag=0;
    break;
    case ONEBOT_DIG_DISP:
      TYPE_NUM=TYPE_DIG_DISP;
      METHOD_NUM=METHOD_DIG_DISP; 
      AIQI_Data_Direction_Flag=0;
    break;
    case ONEBOT_BOARD:
      TYPE_NUM=TYPE_BOARD;
      METHOD_NUM=METHOD_RGB_LED;
      AIQI_Data_Direction_Flag=0;
    break;
    case ONEBOT_PIR:
      TYPE_NUM=TYPE_PIR;  
      METHOD_NUM=METHOD_PIR;
      AIQI_Data_Direction_Flag=1;
    break;
    case ONEBOT_ST:
      TYPE_NUM=TYPE_SMOKE;
      METHOD_NUM=METHOD_SMOKE; 
      AIQI_Data_Direction_Flag=1;
    break;
    case ONEBOT_RR:
      TYPE_NUM=TYPE_GateMagnetic;  
      METHOD_NUM=METHOD_GateMagnetic;
      AIQI_Data_Direction_Flag=1;
    break;
    case ONEBOT_LE:
      TYPE_NUM=TYPE_LaserEmission; 
      METHOD_NUM=METHOD_LaserEmission; 
      AIQI_Data_Direction_Flag=0;
    break;
    case ONEBOT_LR:
      TYPE_NUM=TYPE_LaserInduction; 
      METHOD_NUM=METHOD_LaserInduction;
      AIQI_Data_Direction_Flag=1;
    break;
    case ONEBOT_AP:
      TYPE_NUM=TYPE_AtmosphericPressure; 
      METHOD_NUM=METHOD_AtmosphericPressure; 
      AIQI_Data_Direction_Flag=1;
    break;
    case ONEBOT_TH:
      TYPE_NUM=TYPE_TEMP_HUMI;
      METHOD_NUM=METHOD_TEMP;
      AIQI_Data_Direction_Flag=1;
    break;
  }
  AIQI_SPI_Init();
}

void AIQI_SPI_Init(void)
{
   pinMode(MISO, OUTPUT);
   SPCR |= _BV(SPE);
   SPISettings(15000000, MSBFIRST, SPI_MODE0);
   SPI.attachInterrupt();
}

ISR (SPI_STC_vect)
{
  uint8_t Comm,Verify,i;
  uint8_t Rvalue[3];
  uint8_t Tvalue[3]={0};
  
  Comm = SPI.transfer(0xAC);
  SPI.transfer(METHOD_NUM);
  SPI.transfer(AIQI_Data_Direction_Flag);
  switch(Comm)
  {
    case 0x01:{
      Tvalue[0]=TYPE_NUM;
        for(i=0;i<3;i++)
      {
        Rvalue[i] = SPI.transfer(Tvalue[i]);
      }
      break;
    }
    case 0x04:{
      memcpy(Tvalue,AIQI_Write_Bytes,3);
      for(i=0;i<3;i++)
      {
        Rvalue[i] = SPI.transfer(Tvalue[i]);
      }
      break; 
    }
  }
    Verify=(Rvalue[0]^Rvalue[1]^Rvalue[2]^Tvalue[0]^Tvalue[1]^Tvalue[2]);
    if(SPI.transfer(Verify)==Verify)
    {
      memcpy(AIQI_Read_Bytes,Rvalue,3);
    }else{
      Serial.println("丢包！");
      //用户可添加丢包执行的函数

    }
  }
