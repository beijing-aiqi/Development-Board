#include "Arduino.h"
#include "SPI.h"
#define  proport          25000 //Tclk/(psc+1)=72000000/(7199+1)
//低音 do re mi fa sol la si
#define  Do_L       ((proport/131)-1)
#define  Re_L       ((proport/147)-1)
#define  Mi_L       ((proport/165)-1)
#define  Fa_L       ((proport/176)-1)
#define  Sol_L      ((proport/196)-1)
#define  La_L       ((proport/220)-1)
#define  Si_L       ((proport/247)-1)
//中音 do re mi fa sol la si
#define  Do_M       ((proport/262)-1)
#define  Re_M       ((proport/296)-1)
#define  Mi_M       ((proport/330)-1)
#define  Fa_M       ((proport/349)-1)
#define  Sol_M      ((proport/392)-1)
#define  La_M       ((proport/440)-1)
#define  Si_M       ((proport/494)-1)
//高音 do re mi fa sol la si
#define  Do_H       ((proport/523)-1)
#define  Re_H       ((proport/587)-1)
#define  Mi_H       ((proport/659)-1)
#define  Fa_H       ((proport/699)-1)
#define  Sol_H      ((proport/784)-1)
#define  La_H       ((proport/880)-1)
#define  Si_H       ((proport/988)-1)

typedef struct
{
  uint16_t mName; //元素1，音名，取值是上边的三种音名，低中高（1~7),0表是休止符
  uint16_t mTime; //元素2，时值，取值范围是全音符，半分音符等等
}AIQI_Score;

void AIQI_Play_Music(AIQI_Score *music);
void AIQI_Beep_Close(void);
void AIQI_Play_Notes(uint8_t Note, uint8_t Tone);
/****************************************************************************
函数功能：关闭蜂鸣器
输入参数：None  
输出参数：None
备    注：
*****************************************************************************/
void AIQI_Beep_Close()
{
	digitalWrite(6,LOW);//不发声音
}
/****************************************************************************
函数功能：播放指定音乐
输入参数：music：乐谱
输出参数：None
备    注：每次调用播放一整首音乐，播放中无法中断
*****************************************************************************/
void AIQI_Play_Music(AIQI_Score *music)
{
        int i,j;
        noInterrupts();//关闭中断
        for(i=0; i>=0; i++)
          {
            if(music[i].mName == 0) break;
						if(music[i].mName != 0)
						{
							for(j=0;j<music[i].mTime*300;j++)
              {
                digitalWrite(6, HIGH);
                delayMicroseconds(music[i].mName*4);
                digitalWrite(6, LOW);
                delayMicroseconds(music[i].mName*4);
              }
              AIQI_Beep_Close();
						}
          }
				 AIQI_Beep_Close();
         interrupts();//开启中断
}
/****************************************************************************
函数功能：播放指定音符
输入参数：Do~Si 低中高音
输出参数：None
备    注：
*****************************************************************************/
void AIQI_Play_Notes(uint8_t Note, uint8_t Tone)
{
        int i,j;
        int Notes[3][7]={{Do_L,Re_L,Mi_L,Fa_L,Sol_L,La_L,Si_L},
                         {Do_M,Re_M,Mi_M,Fa_M,Sol_M,La_M,Si_M},
                         {Do_H,Re_H,Mi_H,Fa_H,Sol_H,La_H,Si_H}};
        if(Note<=0)Note=0;
        if(Note>=7)Note=7;
        if(Tone<=0)Note=0;
        if(Tone>=3)Note=3;
        noInterrupts();//关闭中断
        for(j=0;j<300;j++)
        {
          digitalWrite(6, HIGH);
          delayMicroseconds(Notes[Tone-1][Note-1]*4);
          digitalWrite(6, LOW);
          delayMicroseconds(Notes[Tone-1][Note-1]*4);
        }
        AIQI_Beep_Close();
        interrupts();//开启中断
}
