#include "AIQI_BSP_Driver.h"//AIQI开发板头文件
#include "OneDuino_Buzzer.h"//蜂鸣器模块驱动头文件
#include "Music.h"//存放音乐代码文件

void setup()
{
   AIQI_BSP_Init(ONEBOT_BEEP);//AIQI硬件驱动初始化 选择传感器
   pinMode(6, OUTPUT);//IO口初始化
}
void loop()
{
   if(AIQI_Read_Bytes[0] > 10)//若十位（音高）不为0
   {
    AIQI_Play_Notes(AIQI_Read_Bytes[0]%10,AIQI_Read_Bytes[0]/10);//十位作为音高，个位作为音调
   }
   AIQI_Read_Bytes[0] = 0;//清除主控发送的数据
}
