from microbit import * # 加载核心支持包
import AIQI_DRV_TemperatureHumidity # 加载模块支持包
AIQI_DRV_TemperatureHumidity.AIQI_DRV_Init() # 开发板初始化
i2c.init(freq=100000, sda=pin20, scl=pin19) # IIC初始化
AHT10_i2cAddr = 0x38 # 温湿度传感器IIC地址
AHT10_cmdInit = bytearray([0xe1, 0x08, 0x00])  # 初始化指令
AHT10_cmdGetData = bytearray([0xac, 0x33, 0x00])  # 测量数据指令
AHT10_cmdReset = bytearray([0xba])  # 软复位指令
i2c.write(AHT10_i2cAddr, AHT10_cmdReset) # 向温湿度传感器写复位指令
sleep(20) # 等待复位完成
i2c.write(AHT10_i2cAddr, AHT10_cmdInit) # 向温湿度传感器写初始化指令
while 1:
    i2c.write(AHT10_i2cAddr, AHT10_cmdGetData) # 向温湿度传感器写测量指令
    sleep(100) # 等待测量完成
    buf_Data = i2c.read(0x38, 6) # 读取测量结果
    TEMP_Data = (((buf_Data[3] & 0x0f) << 16)
                | buf_Data[4] << 8 | buf_Data[5]) # 解析温度测量结果
    TEMP_Int = int(TEMP_Data / 1048756.0 * 200 - 50) # 温度数据计算
    TEMP_Dec = int((TEMP_Data / 1048756.0 * 200 - 50 - TEMP_Int) * 100)
    RH_Data = ((buf_Data[1] << 12) | (buf_Data[2] << 4)
                | ((buf_Data[3] >> 4) & 0x0f)) # 解析湿度测量结果
    RH_Int = int(RH_Data / 1048756.0 * 100) # 湿度数据计算
    RH_Dec = int((RH_Data / 1048756.0 * 100 - RH_Int) * 100)
    AIQI_DRV_TemperatureHumidity.RH_State(0, RH_Int, RH_Dec) # 将湿度值发送给主控
    AIQI_DRV_TemperatureHumidity.TEMP_State(0, TEMP_Int, TEMP_Dec) # 将温度值发送给主控
