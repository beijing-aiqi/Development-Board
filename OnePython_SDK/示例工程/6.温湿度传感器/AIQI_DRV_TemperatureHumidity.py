from microbit import *
import time
byte00 = bytearray([0x00])
DATA00 = byte00
DATA01 = byte00
DATA02 = byte00
TP_NUM = 0x59
MT_NUM = 0x05
def AIQI_DRV_Init():
    display.off()
    spi.init(baudrate=2000000, bits=8, mode=0, sclk=pin13,
             mosi=pin15, miso=pin14)
    Device_REG()
    time.sleep_ms(500)

def Device_REG():
    global TP_NUM
    global MT_NUM
    XOR_Num = (0xaa ^ 0x55 ^ 0x00 ^ 0x07 ^ TP_NUM ^ MT_NUM)
    REG_Infor = bytearray([0xaa, 0x55, 0x00, 0x07, XOR_Num, TP_NUM, MT_NUM])
    pin16.write_digital(1)
    spi.write(REG_Infor)
    pin16.write_digital(0)

def GetData(GetMT_NUM, Data00, Data01, Data02):
    XOR_Num = (0xdd ^ 0x22 ^ 0x00 ^ 0x09 ^ GetMT_NUM ^
               Data00 ^ Data01 ^ Data02)
    DataInfor = bytearray([0xdd, 0x22, 0x00, 0x09, XOR_Num,
                           GetMT_NUM, Data00, Data01, Data02])
    pin16.write_digital(1)
    spi.write(DataInfor)
    pin16.write_digital(0)
    time.sleep_ms(50)

def SetData(SetMT_NUM):
    global DATA00
    global DATA01
    global DATA02
    buf_SPIread = [byte00, byte00, byte00, byte00, byte00,
                   byte00, byte00, byte00, byte00]
    XOR_Num = (0xcc ^ 0x33 ^ 0x00 ^ 0x06 ^ SetMT_NUM)
    DataInfor = bytearray([0xcc, 0x33, 0x00, 0x06, XOR_Num, SetMT_NUM])
    pin16.write_digital(1)
    spi.write(DataInfor)
    pin16.write_digital(0)
    time.sleep_ms(50)
    for i in range(0, 9):
        pin16.write_digital(1)
        buf_SPIread[i] = spi.read(1)
        pin16.write_digital(0)
        time.sleep_ms(50)
    buf_SPIread[0] = int.from_bytes(buf_SPIread[0], 'little')
    buf_SPIread[1] = int.from_bytes(buf_SPIread[1], 'little')
    buf_SPIread[2] = int.from_bytes(buf_SPIread[2], 'little')
    buf_SPIread[3] = int.from_bytes(buf_SPIread[3], 'little')
    buf_SPIread[4] = int.from_bytes(buf_SPIread[4], 'little')
    buf_SPIread[5] = int.from_bytes(buf_SPIread[5], 'little')
    buf_SPIread[6] = int.from_bytes(buf_SPIread[6], 'little')
    buf_SPIread[7] = int.from_bytes(buf_SPIread[7], 'little')
    buf_SPIread[8] = int.from_bytes(buf_SPIread[8], 'little')
    XOR_Num = (buf_SPIread[0] ^ buf_SPIread[1] ^ buf_SPIread[2] ^
               buf_SPIread[3] ^ buf_SPIread[5] ^
               buf_SPIread[6] ^ buf_SPIread[7] ^ buf_SPIread[8])
    if (XOR_Num == buf_SPIread[4]) and (buf_SPIread[3] != 0):
        DATA00 = buf_SPIread[6]
        DATA01 = buf_SPIread[7]
        DATA02 = buf_SPIread[8]
    return (DATA00, DATA01, DATA02)

def TEMP_State(Data00, Data01, Data02):  # 0x59 0x05
    GetData(0x05, Data00, Data01, Data02)
    time.sleep_ms(500)

def RH_State(Data00, Data01, Data02):  # 0x59 0x06
    GetData(0x06, Data00, Data01, Data02)
    time.sleep_ms(500)