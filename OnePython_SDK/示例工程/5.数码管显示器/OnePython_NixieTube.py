from microbit import * # 加载核心支持包
import AIQI_DRV_NixieTube # 加载模块支持包
import NumDisplay # 加载模块驱动包
AIQI_DRV_NixieTube.AIQI_DRV_Init() # 开发板初始化
NumDisplay.NumDisp_Level(8)  # 设置亮度等级：1-8
Num = 0 # 变量初始化
while 1:
    buf_DisData = AIQI_DRV_NixieTube.DIG_DISP() # 获取主控发送的数据
    varDIS00 = int.from_bytes(buf_DisData[0], 'little') # 数据类型转换为byte
    varDIS01 = int.from_bytes(buf_DisData[1], 'little') # 数据类型转换为byte
    varDIS = varDIS00*256 + varDIS01 # 将两个8位数据组合为16位数据
    NumDisplay.NumDisp_Num(varDIS) # 数码管显示数字

