from microbit import *
SG = [0x3f, 0x06, 0x5b, 0x4f, 0x66, 0x6d, 0x7d, 0x07, 0x7f, 0x6f]   # 段码表
DIG = [0x68, 0x6a, 0x6c, 0x6e]  # 数码管位数
LEV = [0x11, 0x21, 0x31, 0x41, 0x51, 0x61, 0x71, 0x01]  # 8段显示亮度等级
class NUMDISP(object):
    def Delay_iic(t):
        while t > 0:
            t = t - 1

    def I2CStart():
        pin3.write_digital(1)
        pin0.write_digital(1)
        NUMDISP.Delay_iic(5)
        pin0.write_digital(0)
        NUMDISP.Delay_iic(2)

    def I2CAsk():
        NUMDISP.Delay_iic(5)
        pin3.write_digital(1)
        NUMDISP.Delay_iic(5)
        pin3.write_digital(0)
        NUMDISP.Delay_iic(5)
        pin3.write_digital(0)

    def I2CStop():
        pin3.write_digital(1)
        pin0.write_digital(0)
        NUMDISP.Delay_iic(5)
        pin0.write_digital(1)

    def I2CWrByte(Byte):
        pin3.write_digital(0)
        Num = 8
        while Num > 0:
            NUMDISP.Delay_iic(2)
            if Byte & 0x80:
                pin0.write_digital(1)
            else:
                pin0.write_digital(0)
            Byte = Byte << 1
            pin3.write_digital(0)
            NUMDISP.Delay_iic(5)
            pin3.write_digital(1)
            NUMDISP.Delay_iic(5)
            pin3.write_digital(0)
            Num = Num - 1

    def Aip650_Set(ADD, DAT):
        NUMDISP.I2CStart()
        NUMDISP.I2CWrByte(ADD)
        NUMDISP.I2CAsk()
        NUMDISP.I2CWrByte(DAT)
        NUMDISP.I2CAsk()
        NUMDISP.I2CStop()

def NumDisp_OneBit(D, N, P):
    if P == 1:
        NUMDISP.Aip650_Set(DIG[D - 1], SG[N] | 0x80)
    if P == 0:
        NUMDISP.Aip650_Set(DIG[D - 1], SG[N])

def NumDisp_Level(L):
    NUMDISP.Aip650_Set(0x48, LEV[L - 1])

def NumDisp_Num(Num):
    one = int(Num / 100)
    two = int(Num % 100 / 10)
    three = int(Num % 10)
    NumDisp_OneBit(1, one, 0)
    NumDisp_OneBit(2, two, 0)
    NumDisp_OneBit(3, three, 0)