from microbit import * # 加载核心支持包
import AIQI_DRV_Light # 加载模块支持包
AIQI_DRV_Light.AIQI_DRV_Init() # 开发板初始化
while 1:
    Voltage = ((pin4.read_analog())/1024*3300) # 获取传感器读值
    Voltage = (int)(Voltage) # 数据类型转换
    AIQI_DRV_Light.Light_State((Voltage >> 8), (Voltage & 0xff), 0) #将光照值发送给主控

