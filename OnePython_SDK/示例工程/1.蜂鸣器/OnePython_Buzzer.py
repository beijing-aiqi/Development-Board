from microbit import * # 加载核心支持包
import AIQI_DRV_Buzzer # 加载模块支持包
AIQI_DRV_Buzzer.AIQI_DRV_Init() # 开发板初始化
while 1:
    paraA = AIQI_DRV_Buzzer.BeepState() # 获取主控发送的数据
    if paraA != 0:
        paraA = 1023-paraA * 5 # 数据处理 转换为Python PWM控制值
        pin1.set_analog_period_microseconds(1000) # 设置PWM周期1ms
        pin1.write_analog(paraA) # 引脚输出PWM信号
    else:
        pin1.write_digital(1) # 关闭引脚PWM输出