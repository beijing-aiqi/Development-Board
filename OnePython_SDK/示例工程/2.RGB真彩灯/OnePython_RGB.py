from microbit import * # 加载核心支持包
import AIQI_DRV_RGB # 加载模块支持包
AIQI_DRV_RGB.AIQI_DRV_Init() # 开发板初始化
while 1:
    (paraR, paraG, paraB) = AIQI_DRV_RGB.RGB_LED_State() # 获取主控发送的数据
    paraR = 1023 - paraR*4  # 数据处理 转换为Python PWM控制值
    pin7.write_analog(paraR) # ‘R’引脚输出控制信息
    paraG = 1023 - paraG*4
    pin1.write_analog(paraG) # ‘G’引脚输出控制信息
    paraB = 1023 - paraB*4
    pin2.write_analog(paraB) # ‘B’引脚输出控制信息